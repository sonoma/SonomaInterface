import java.io.IOException;
import java.io.OutputStream;

import javafx.scene.control.TextArea;
import javafx.application.Platform;


public class CustomOutputStream extends OutputStream {
    private TextArea textArea;

    public CustomOutputStream(TextArea textArea) {
        this.textArea = textArea;
    }

    @Override
    public void write(int b) throws IOException {
        // redirects data to the text area
        // textArea.appendText(String.valueOf((char)b));

        Platform.runLater(new Runnable(){
          String text = String.valueOf((char)b);
          public void run(){
            textArea.appendText(text);
          }
        });
   }
}
