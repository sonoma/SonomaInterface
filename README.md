## Sonoma User Interface

Main App runs on Java 8 64bits. But on Raspberry Pi, we need to trick.

First, you have to find a valid `jfxrst.jar` version. (Check javafx embedded or something like that).

Then for compiling:

```
$ javac --classpath jars/jfxrst.jar LayoutSonoma.java
$ java -cp ".:jars/jfxrst.jar" LayoutSonoma

```

(Tips: You can write those options into `makeit.sh`)
