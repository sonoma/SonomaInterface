import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.Screen;
import javafx.scene.shape.*;

import java.io.PrintStream;
import java.lang.ProcessBuilder.Redirect;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.util.Date;




/**
 * Sample application that shows examples of the different layout panes
 * provided by the JavaFX layout API.
 * The resulting UI is for demonstration purposes only and is not interactive.
 */


public class LayoutSonoma extends Application {

    private PrintStream standardOut;
    private ProcessBuilder pb;
    private Circle circle;
    private Process process;



    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(LayoutSonoma.class, args);
    }

    public void setCircleColor(Color c)
    {
        circle.setFill(c);
    }

    @Override
    public void start(Stage stage) {


// Use a border pane as the root for scene
        BorderPane border = new BorderPane();

        HBox hbox = addHBox();
        border.setTop(hbox);
        border.setLeft(addVBox());

// Add a stack to the HBox in the top region
        addStackPane(hbox);

// To see only the grid in the center, uncomment the following statement
// comment out the setCenter() call farther down
//        border.setCenter(addGridPane());

// Choose either a TilePane or FlowPane for right region and comment out the
// one you aren't using
        border.setBottom(addFlowPane());
//        border.setRight(addTilePane());

// To see only the grid in the center, comment out the following statement
// If both setCenter() calls are executed, the anchor pane from the second
// call replaces the grid from the first call


        Scene scene = new Scene(border);

        stage.setScene(scene);
        stage.setTitle("Sonoma Control Panel");
        // stage.setFullScreen(true);
        stage.show();
    }

/*
 * Creates an HBox with two buttons for the top region
 */

    private HBox addHBox() {

        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(10);   // Gap between nodes
        hbox.setStyle("-fx-background-color: #336699;");

        Button buttonStart = new Button("Start");
        buttonStart.setStyle("-fx-background-color: #59D286; ");

        buttonStart.setOnAction((event) -> {

          try
          {
            process = pb.start();
            final Thread ioThread = new Thread() {
              @Override
              public void run() {
                try {
                  final BufferedReader reader = new BufferedReader(
                  new InputStreamReader(process.getInputStream()));
                  String line = null;
                  while ((line = reader.readLine()) != null) {
                    if(line.toLowerCase().contains("default".toLowerCase()))
                    {
                      setCircleColor(Color.RED);
                    }
                    System.out.println(line);

                  }
                  reader.close();
                } catch (final Exception e) {
                  e.printStackTrace();
                }
              }
            };
            ioThread.start();

            // process.waitFor();

          }catch(Exception e)
          {
            e.printStackTrace();
          }

        });

        buttonStart.setPrefSize(100, 20);

        Button buttonStop = new Button("Stop");
        buttonStop.setStyle("-fx-background-color: #FE6447; ");

        buttonStop.setOnAction((event) -> {
          try
          {
            process.destroy();
          }catch(Exception e)
          {
            e.printStackTrace();
          }
        });

        buttonStop.setPrefSize(100, 20);

        Button buttonDisable = new Button("Clear default");
        buttonDisable.setStyle("-fx-background-color: #a1887f; ");
        buttonDisable.setPrefSize(150, 20);

        buttonDisable.setOnAction((event) -> {
          setCircleColor(Color.GREEN);
        });

        Button buttonShutdown = new Button("Shutdown");
        buttonShutdown.setStyle("-fx-background-color: #ffff00; ");
        buttonShutdown.setPrefSize(150, 20);

        buttonShutdown.setOnAction((event) -> {
          try
          {
            Runtime.getRuntime().exec("shutdown now");
          }catch(Exception e )
          {
            e.printStackTrace();
          }

        });

        hbox.getChildren().addAll(buttonStart, buttonStop, buttonDisable, buttonShutdown);

        return hbox;
    }


    private VBox addVBox() {

        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10)); // Set all sides to 10
        vbox.setSpacing(8);              // Gap between nodes

        Text title = new Text("Raspberry Pi Control Panel");
        title.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        vbox.getChildren().add(title);

        Text status = new Text("Current status: ");
        vbox.getChildren().add(status);

        circle = new Circle();
        circle.setCenterX(100.0f);
        circle.setCenterY(100.0f);
        circle.setRadius(15.0f);

        circle.setFill(Color.GREEN);

        vbox.getChildren().add(circle);

        TextArea consoleOutput = new TextArea();
        consoleOutput.setEditable(false);


        PrintStream printStream = new PrintStream(new CustomOutputStream(consoleOutput));

        // keeps reference of standard output stream
        standardOut = System.out;

        // re-assigns standard output stream and error output stream
        System.setOut(printStream);
        System.setErr(printStream);

        pb = new ProcessBuilder("./hello");
        pb.redirectErrorStream(true);

        vbox.getChildren().add(consoleOutput);


        return vbox;
    }

/*
 * Uses a stack pane to create a help icon and adds it to the right side of an HBox
 *
 * @param hb HBox to add the stack to
 */
    private void addStackPane(HBox hb) {

        StackPane stack = new StackPane();
        Rectangle helpIcon = new Rectangle(30.0, 25.0);
        helpIcon.setFill(new LinearGradient(0,0,0,1, true, CycleMethod.NO_CYCLE,
            new Stop[]{
            new Stop(0,Color.web("#4977A3")),
            new Stop(0.5, Color.web("#B0C6DA")),
            new Stop(1,Color.web("#9CB6CF")),}));
        helpIcon.setStroke(Color.web("#D0E6FA"));
        helpIcon.setArcHeight(3.5);
        helpIcon.setArcWidth(3.5);

        Text helpText = new Text("?");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));

        stack.getChildren().addAll(helpIcon, helpText);
        stack.setAlignment(Pos.CENTER_RIGHT);
        // Add offset to right for question mark to compensate for RIGHT
        // alignment of all nodes
        StackPane.setMargin(helpText, new Insets(0, 10, 0, 0));

        hb.getChildren().add(stack);
        HBox.setHgrow(stack, Priority.ALWAYS);

    }


    private FlowPane addFlowPane() {

        FlowPane flow = new FlowPane();

        WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();

        flow.setPadding(new Insets(5, 0, 5, 0));
        flow.setVgap(4);
        flow.setHgap(4);
        flow.setStyle("-fx-background-color: DAE6F3;");

        // Text chartTitle = new Text("Web server");
        // chartTitle.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        // flow.getChildren().add(chartTitle);

        flow.getStyleClass().add("browser");
        // load the web page
        webEngine.load("http://dev.cultr.jed.st");
        // flow.getChildren().add(browser);

        return flow;
    }

/*
 * Creates a horizontal (default) tile pane with eight icons in four rows
 */
    private TilePane addTilePane() {

        TilePane tile = new TilePane();
        tile.setPadding(new Insets(5, 0, 5, 0));
        tile.setVgap(4);
        tile.setHgap(4);
        tile.setPrefColumns(2);
        tile.setStyle("-fx-background-color: DAE6F3;");
        return tile;
    }

/*
 * Creates an anchor pane using the provided grid and an HBox with buttons
 *
 * @param grid Grid to anchor to the top of the anchor pane
 */
    private AnchorPane addAnchorPane(GridPane grid) {

        AnchorPane anchorpane = new AnchorPane();

        Button buttonShutdown = new Button("Shutdown");

        buttonShutdown.setOnAction((event) -> {
          try
          {
            Runtime.getRuntime().exec("true");
          }catch(Exception e)
          {
            e.printStackTrace();
          }
        });

        Button buttonRestart = new Button("Restart");


        buttonRestart.setOnAction((event) -> {
          /* START PROCESS */
          try
          {
            Runtime.getRuntime().exec("true");
          }catch(Exception e)
          {
            e.printStackTrace();
          }

        });

        HBox hb = new HBox();
        hb.setPadding(new Insets(0, 10, 10, 10));
        hb.setSpacing(10);
        hb.getChildren().addAll(buttonShutdown, buttonRestart);

        anchorpane.getChildren().addAll(grid,hb);
        // Anchor buttons to bottom right, anchor grid to top
        AnchorPane.setBottomAnchor(hb, 8.0);
        AnchorPane.setRightAnchor(hb, 5.0);
        AnchorPane.setTopAnchor(grid, 10.0);

        return anchorpane;
    }
}
